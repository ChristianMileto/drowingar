//
//  ViewController.swift
//  Drowing
//
//  Created by Christian on 03/12/2019.
//  Copyright © 2019 Christian. All rights reserved.
//

import UIKit
import RealityKit
import ARKit
import ReplayKit




class ViewController: UIViewController,RPPreviewViewControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate{
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var Segment: UISegmentedControl!
    
    @IBOutlet weak var colorbutton: UIButton!
    @IBOutlet weak var arView: ARSCNView!
    public var tap = 0
    var color = UIColor.init()
    var control = false
    var i = 0

    @IBAction func takephoto(_ sender: Any) {
       let snapshot = arView.snapshot()
        UIImageWriteToSavedPhotosAlbum(snapshot, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
      
        
    }
    @IBAction func video(_ sender: Any) {
       
        let videe = vide(view: ViewController.init())
        videe.startRecording()
    
    }
   
    @IBAction func stopvideo(_ sender: Any) {

               print("STOP RECORDING")
                
               RPScreenRecorder.shared().stopRecording { [unowned self] (review, error) in
               print("Stopped recording");
               guard error == nil else {
               print("There was an error stopping the recording.")
               return
               }
               guard review != nil else {
               print("Preview controller is not available.")
               return
               }
               if let unwrappedPreview = review {
               
                unwrappedPreview.previewControllerDelegate = self
                
                self.present(unwrappedPreview, animated: true)
                    }
                }
                
        
       
    }
   
    
    let configuration = ARWorldTrackingConfiguration()
    

    override func viewDidLoad() {
        super.viewDidLoad()
 
        collectionView.isHidden = true
        // Load the "Box" scene from the "Experience" Reality File
    
        arView.showsStatistics = false
        
        // Add the box anchor to the scene
        arView.session.run(configuration)
        arView.delegate = self
        
        
        
    }
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
    }
    @IBAction func boh(_ sender: Any) {
          self.arView.scene.rootNode.enumerateChildNodes { (node, stop) in
              node.removeFromParentNode()
          }
         
         
      }
      
      @IBAction func back(_ sender: Any) {
          self.arView.scene.rootNode.enumerateChildNodes{(node,stop) in
              if(node.name == String(i-1))
              {
                
                  node.removeFromParentNode()
                  i-=1
              }
          }
      }
    @IBAction func colorD(_ sender: Any) {
        
       
        if tap == 0 {
            self.collectionView.isHidden = false
             tap = tap + 1
        }
       else if tap == 1
        {
            self.collectionView.isHidden = true
             tap = 0
        }
        
        
    }

    
    
    let items = [UIColor.green,UIColor.red,UIColor.gray,UIColor.black,UIColor.blue,UIColor.brown,UIColor.yellow,UIColor.cyan,UIColor.orange]
    
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return items.count
        
     }
     
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        cell.Circle.tintColor = items[indexPath.item]

         return cell
     }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
               control = true
         colorbutton.tintColor = items[indexPath.item]
         color = items[indexPath.item]
        collectionView.isHidden = true
    }
    @IBAction func Cameras(_ sender: Any) {
        if Segment.selectedSegmentIndex == 1
        {
            
        }
    }
    
        
  
}

extension ViewController : ARSCNViewDelegate
{
   
    
  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
for touch: AnyObject in touches {
      //1. Get The Current Touch Location
      guard let currentTouchPoint = touches.first?.location(in: self.arView),
          //2. Perform An ARHitTest For Detected Feature Points
          let featurePointHitTest = self.arView.hitTest(currentTouchPoint, types: .featurePoint).first else { return }

      //3. Prendi le coordinate del mondo
      let worldCoordinates = featurePointHitTest.worldTransform

      //4. dai una forma alla sfera
    let sphereNode = SCNNode()
         let sphereNodeGeometry = SCNSphere(radius: 0.01)
  

      //5. Generate A Random Colour For The Node's Geometry

      sphereNode.geometry = sphereNodeGeometry
    
    //setto il colore della sfera se è stato scelto
    if (control == true)
    {
     sphereNode.geometry?.firstMaterial?.diffuse.contents = color
    }
      
      //6. Position & Add It To The Scene Hierachy
       sphereNode.position = SCNVector3(worldCoordinates.columns.3.x,  worldCoordinates.columns.3.y,  worldCoordinates.columns.3.z)
          
     self.arView.scene.rootNode.addChildNode(sphereNode)
     sphereNode.name = String(i)
            i = i+1
         

  }
    func renderer(_ renderer: SCNSceneRenderer, willRenderScene scene: SCNScene, atTime time: TimeInterval) {
    
          
           
       }
    }
    
}
